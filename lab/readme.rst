
Laboratorium #9
===============

1. W katalogu microblog uzupełnić pliki ``models.py``, ``views.py`` oraz ``templates/list.pt`` w celu utworzenia
   podstawowej wersji aplikacji do mikroblogowania.

2. W pliku ``tests.py`` dodać testy dla wszystkich modeli i widoków.
