# -*- coding: utf-8 -*-

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config

from .models import Blog, Post, Comment, Comment2
from time import time
from docutils.core import publish_parts


# Główna strona
@view_config(context='.models.Blog', renderer='templates/list.pt')
def view_blog(context, request):
    #blog_ = context
    blog_list=context.items()
    sorted_blog_list= sorted(blog_list, key=lambda tup: tup[1].created, reverse=True)[:6]
    """ Wyświetla listę n ostatnich wpisów """
    add_url = request.resource_url(context, 'add')
    return dict(blog=sorted_blog_list, add_url=add_url)
    pass


@view_config(context='.models.Post', renderer='templates/view.pt')
def view_post(context, request):
    post = context
    content = publish_parts(context.content, writer_name='html')['html_body']
    comments = context.items()
    add_comment_url = request.resource_url(context, 'add_comment')
    return dict(post=post, content=content, comments=comments, add_comment_url=add_comment_url)
    """ Wyświetla szczegółowo dany wpis """
    pass

@view_config(context='.models.Comment', renderer='templates/viewcomment.pt')
def view_comment(context, request):
    post = context
    content = publish_parts(context.content, writer_name='html')['html_body']
    comments = context.items()
    add_comment_url = request.resource_url(context, 'add_comment2')
    return dict(post=post, comments=comments, content=content, add_comment_url=add_comment_url)
    """ Wyświetla szczegółowo dany wpis """
    pass

@view_config(context='.models.Comment2', renderer='templates/viewcomment2.pt')
def view_comment2(context, request):
    post = context
    content = publish_parts(context.content, writer_name='html')['html_body']
    #comments = context.items()
    #add_comment_url = request.resource_url(context, 'add_comment2')
    return dict(post=post, content=content)
    """ Wyświetla szczegółowo dany wpis """
    pass


@view_config(name='add', context='.models.Blog')
def add_post(context, request):
    # Pierwszy element ścieżki po bieżącym kontekście i widoku
    #pagename = request.subpath[0]
    if request.method == 'POST':
        content = request.POST['content']
        title = request.POST['title']
        post = Post(title, content)
        post.__name__=title
        post.__parent__=context
        # Dodanie nowej strony do kolekcji stron w klasie Wiki
        context[title] = post
        #koment = Comment('test', 'to bardzo glupi koment')
        #koment.__name__ = 'test'
        #koment.__context__ = post
        #post['test'] = koment
        return HTTPFound(location=request.resource_url(context))
    #save_url = request.resource_url(context, 'add_page', pagename)
    #page = Page('')
    #page.__name__ = pagename
    #page.__parent__ = context
    #return dict(page=page, save_url=save_url)
    """ Dodaje nowy wpis """
    pass

@view_config(name='add_comment', context='.models.Post', renderer='templates/addcomment.pt')
def add_comment(context, request):
    post = context
    add_comment_url = request.resource_url(context, 'add_comment')
    #comment = context.__parent__
    if request.method == 'POST':
        content = request.POST['content']
        title = request.POST['title']
        #level = 1
        #post = Post(title, content)
        #post.__name__=title
        comment = Comment(title, content)
        comment.__name__ = title
        comment.__parent__=context
        # Dodanie nowej strony do kolekcji stron w klasie Wiki
        context[title] = comment
        return HTTPFound(location=request.resource_url(context))
    #add_comment_url = request.resource_url(context, 'add_comment')
    return dict(add_comment_url = add_comment_url)
    """ Wyświetla szczegółowo dany wpis """
    pass

@view_config(name='add_comment2', context='.models.Comment', renderer='templates/addcomment2.pt')
def add_comment2(context, request):
    parent_comment = context
    add_comment2_url = request.resource_url(context, 'add_comment2')
    #comment = context.__parent__
    if request.method == 'POST':
        content = request.POST['content']
        title = request.POST['title']
        #level = 1
        #post = Post(title, content)
        #post.__name__=title
        comment = Comment2(title, content)
        comment.__name__ = title
        comment.__parent__=context
        # Dodanie nowej strony do kolekcji stron w klasie Wiki
        context[title] = comment
        return HTTPFound(location=request.resource_url(context))
        #add_comment_url = request.resource_url(context, 'add_comment')
    return dict(add_comment2_url = add_comment2_url)
    """ Wyświetla szczegółowo dany wpis """
    pass

