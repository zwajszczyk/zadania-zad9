# -*- coding: utf-8 -*-

from persistent.mapping import PersistentMapping
from persistent import Persistent
import datetime
import time



class Blog(PersistentMapping):
    """ Klasa dla kontenera """
    # Należy ją odpowiednio zainicjalizować
    __name__ = None
    __parent__ = None


class Post(PersistentMapping):
    """ Klasa dla pojedynczego wpisu """
    # Należy dorobić odpowiedni konstruktor
    def __init__(self, title, content):
        super(Post,self).__init__()
        self.title=str(title)
        self.content=str(content)
        self.created=datetime.datetime.now()
        #self.comments = []
        #self.comments_count = 0

    def __setitem__(self, key, value):
        super(Post,self).__setitem__(key,value)
        value.__name__=key
        value.__parent__=self

    def comment_count(self):
        count = len(self.items())
        for item in self.items():
            count=count+len(item[1].items())
        return count




class Comment(PersistentMapping):
    def __init__(self, title, content):
        super(Comment,self).__init__()
        self.title = str(title)
        #self.level = int(level)
        self.content = str(content)
        self.created=datetime.datetime.now()
        #self.comments = []
        #self.comments_count = 0

    def __setitem__(self, key, value):
        super(Comment,self).__setitem__(key,value)
        value.__name__=key
        value.__parent__=self


class Comment2(Persistent):
    def __init__(self, title, content):
        super(Comment2,self).__init__()
        self.title = str(title)
        #self.level = int(level)
        self.content = str(content)
        self.created=datetime.datetime.now()
        #self.comments = []
        #self.comments_count = 0



def appmaker(zodb_root):
    if not 'app_root' in zodb_root:
        # Utworzenie głównegp kontenera
        app_root = Blog()
        zodb_root['app_root'] = app_root
        import transaction
        transaction.commit()
    return zodb_root['app_root']
